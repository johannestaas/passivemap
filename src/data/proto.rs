use std::fmt;

pub enum Proto {
    TCP,
    UDP,
    #[allow(dead_code)]
    ICMP
}

impl fmt::Display for Proto {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        #[allow(unreachable_patterns)]
        match &self {
            Proto::TCP => fmt.write_str("tcp")?,
            Proto::UDP => fmt.write_str("udp")?,
            Proto::ICMP => fmt.write_str("icmp")?,
            _ => fmt.write_str("<proto?>")?
        };
        Ok(())
    }
}
