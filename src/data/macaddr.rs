use std::fmt;

#[allow(dead_code)]
pub struct MACAddr {
    pub mac: [u8; 6]
}

impl fmt::Display for MACAddr {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        for (i, &byte) in self.mac.iter().enumerate() {
            if i == 0 {
                write!(fmt, "{:X}", byte)?;
            } else {
                write!(fmt, ":{:X}", byte)?;
            }
        }
        Ok(())
    }
}
