use std::fmt;

#[derive(Debug)]
pub enum EtherType {
    IPV4,
    IPV6,
    ARP,
    RARP
}


impl EtherType {
    #[allow(dead_code)]
    pub fn value(&self) -> u16 {
        match *self {
            EtherType::IPV4 => 0x0800,
            EtherType::IPV6 => 0x86dd,
            EtherType::ARP => 0x0806,
            EtherType::RARP => 0x8035
        }
    }
    pub fn get(val: u16) -> Option<EtherType> {
        match val {
            0x0800 => Some(EtherType::IPV4),
            0x86dd => Some(EtherType::IPV6),
            0x0806 => Some(EtherType::ARP),
            0x8035 => Some(EtherType::RARP),
            _ => None
        }
    }
}

impl fmt::Display for EtherType {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        #[allow(unreachable_patterns)]
        let s = match &self {
            EtherType::IPV4 => "IPV4",
            EtherType::IPV6 => "IPV6",
            EtherType::ARP => "ARP",
            EtherType::RARP => "RARP",
            _ => "UNKNOWN"
        };
        write!(fmt, "{}", s)?;
        Ok(())
    }
}
