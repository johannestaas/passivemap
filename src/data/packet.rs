use std::net::IpAddr;
use super::proto;

pub struct Packet {
    pub proto: proto::Proto,
    pub src: IpAddr,
    pub dst: IpAddr,
    pub sport: Option<u16>,
    pub dport: Option<u16>
}

impl Packet {
    pub fn print(&self) {
        match self.sport {
            Some(_) => {
                println!("{},{},{},{},{}", self.proto, self.src, self.dst, self.sport.unwrap(), self.dport.unwrap());
            },
            None => {
                println!("{},{},{},null,null", self.proto, self.src, self.dst);
            }
        };
    }
}
