use std::net::IpAddr;
use super::data::packet;
use super::data::proto;
use super::data::ether::EtherType;

use etherparse::{SlicedPacket, TransportSlice, InternetSlice, LinkSlice};


pub fn handle_packet(sliced_packet: SlicedPacket) {
    let link = match &sliced_packet.link {
        Some(LinkSlice::Ethernet2(link)) => link,
        None => {
            eprintln!("No Ethernet2 link layer: {:?}", &sliced_packet);
            return;
        }
    };
    let etype = EtherType::get(link.ether_type());
    match etype {
        Some(EtherType::ARP) => {
            eprintln!("ARP: {:?}", sliced_packet.payload);
            return;
        },
        _ => {}
    };
    let ip = match &sliced_packet.ip {
        Some(ip) => ip,
        None => {
            // eprintln!("No ip layer: {:?}", &sliced_packet);
            // ARP: 0x0806
            eprintln!("link: {:?}", link.ether_type());
            return;
        }
    };
    let tport = match &sliced_packet.transport {
        Some(tport) => tport,
        None => {
            // eprintln!("IP header but no transport: {:?}", &sliced_packet);
            // ARP: 0x0806
            eprintln!("link: {:?}", link.ether_type());
            return;
        }
    };
    let (src, dst) = parse_ip(&ip);
    let (proto, sport, dport) = parse_transport(&tport);
    let packet = packet::Packet {
        proto, src: src, dst: dst, sport: Some(sport), dport: Some(dport)
    };
    packet.print();
}

fn parse_ip(slice: &InternetSlice) -> (IpAddr, IpAddr) {
    match slice {
        InternetSlice::Ipv4(ip) => (
            IpAddr::V4(ip.source_addr()),
            IpAddr::V4(ip.destination_addr())
        ),
        InternetSlice::Ipv6(ip, _ext) => (
            IpAddr::V6(ip.source_addr()),
            IpAddr::V6(ip.destination_addr())
        ),
    }
}

fn parse_transport(slice: &TransportSlice) -> (proto::Proto, u16, u16) {
    match slice {
        TransportSlice::Tcp(tcp) => (
            proto::Proto::TCP,
            tcp.source_port(),
            tcp.destination_port()
        ),
        TransportSlice::Udp(udp) => (
            proto::Proto::UDP,
            udp.source_port(),
            udp.destination_port()
        )
    }
}
