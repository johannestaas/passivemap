use colored::*;
use std::error::Error;
use std::fmt;
use std::process::exit;

pub static LOOKUP: &'static str = "failed to lookup device";
pub static CAPTURER: &'static str = "
failed to make packet capturer

You might fix this by invoking with sudo, or better yet, run:

    sudo setcap cap_net_raw=ep /path/to/passivemap
";

#[derive(Debug, Clone)]
pub struct DeviceNotFoundError;

impl fmt::Display for DeviceNotFoundError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "cant find network device")
    }
}

impl Error for DeviceNotFoundError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

/// Pretty prints error messages in color with a custom string appended.
pub fn die<T>(e: Box<Error>, msg: &'static str) -> Option<T> {
    eprintln!("{}", e.to_string());
    eprintln!("{}", msg.red());
    exit(1);
    #[allow(unreachable_code)]
    None
}
