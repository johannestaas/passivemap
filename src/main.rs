use clap::{App, Arg};
use colored::*;
use std::error::Error;
use etherparse::SlicedPacket;

mod data;
mod errors;
mod handler;

type Result<T> = std::result::Result<T, Box<Error>>;

/// Finds the device by interface name passed, eg. eth0 .
/// This only runs if a user passes --iface|-i .
fn find_device_by_name(dev_name: &str) -> Result<pcap::Device> {
    let dev_vec = pcap::Device::list()?;
    for dev in dev_vec {
        if dev.name == *dev_name {
            return Ok(dev);
        }
    }
    Err(Box::new(errors::DeviceNotFoundError))
}

/// Looks up the device either by name or by default.
/// If None is passed, it uses the default lookup which should work fine.
/// If a string is passed from the --iface|-i CLI argument, it will iterate
/// through all the interfaces it can list and pick it or raise errors::DeviceNotFoundError .
fn lookup_device(maybe_dev_name: Option<&str>) -> Result<pcap::Device> {
    match maybe_dev_name {
        Some(dev_name) => Ok(find_device_by_name(dev_name)?),
        None => Ok(pcap::Device::lookup()?),
    }
}

/// Makes a packet capturer from a device.
/// This hooks the capturer to the decided upon network interface and sets it to be promiscuous.
fn make_capturer(dev: pcap::Device) -> Result<pcap::Capture<pcap::Active>> {
    Ok(pcap::Capture::from_device(dev)?.promisc(true).open()?)
}

/// Loops on capturing packets.
/// This is the core passivemap loop that runs on every sniffed packet.
fn capture(capturer: &mut pcap::Capture<pcap::Active>) {
    while let Ok(packet) = capturer.next() {
        // eprintln!("captured packet of size {}", packet.data.len());
        match SlicedPacket::from_ethernet(&packet.data) {
            Err(e) => eprintln!("Parser Error: {:?}", e),
            Ok(sliced_packet) => handler::handle_packet(sliced_packet),
        }
    }
}

fn main() {
    let args = App::new("passivemap")
        .version("0.1")
        .author("Johan Nestaas <johannestaas@gmail.com>")
        .about("A passive network mapper.")
        .arg(
            Arg::with_name("iface")
                .short("i")
                .long("iface")
                .value_name("INTERFACE")
                .help("Sets the network interface to use."),
        )
        .get_matches();
    let maybe_dev_name = args.value_of("iface");
    let dev = match lookup_device(maybe_dev_name) {
        Ok(dev) => {
            eprintln!("using device {}", dev.name.yellow());
            dev
        }
        Err(e) => errors::die(e, errors::LOOKUP).unwrap(),
    };
    let mut capturer = match make_capturer(dev) {
        Ok(capturer) => capturer,
        Err(e) => errors::die(e, errors::CAPTURER).unwrap(),
    };
    capture(&mut capturer);
}
