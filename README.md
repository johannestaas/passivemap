passivemap
==========

A passive network mapper via packet sniffing.

https://docs.rs/etherparse/0.8.2/etherparse/struct.SlicedPacket.html
https://docs.rs/etherparse/0.8.2/etherparse/struct.Ethernet2HeaderSlice.html

http://www.networksorcery.com/enp/protocol/arp.htm

ARP: [0, 1, 8, 0, 6, 4, <0, 1 which is request>, <6 byte mac of router>, <router ip>, 0, 0, 0, 0, 0, 0, <laptop ip>]
ARP: [0, 1, 8, 0, 6, 4, <0, 2 which is reply>, <6 byte mac of laptop>, <laptop ip>, <6 byte mac>, <router ip>]
