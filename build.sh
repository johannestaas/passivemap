#!/bin/bash
set -eu -o pipefail
set -x

cargo build
sudo setcap cap_net_raw,cap_net_admin=eip ${PWD}/target/debug/passivemap
